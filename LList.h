#include <iostream>
#include "Node.h"
using namespace std;

template <typename T>
class LList{
private:
	Node<T>* head;
public:
	LList()
	{
		head=NULL;
	}
	void Insert(const T & value)
	{
		Node<T>*ptr=new Node<T>(value);
		Node<T>*curr=head;
		if(this->head==NULL)
		{
			head=ptr;
		}
		else
		{
			while(curr->next!=NULL)
			{
				curr=curr->next;
			}
			curr->next=ptr;
		}
		

	}
	bool Delete(const T & value)
	{
		if (head==NULL)
		{
			return false;
		}
		else if(head->data==value)
		{
			head=head->next;
			delete (*head);
		}
		else
		{
			Node<T>*ptr1=head;
			Node<T>*ptr2=ptr1->next;
			while(ptr2->data!=value&&ptr2->next!=NULL)
			{
				ptr2=ptr2->next;
				ptr1=ptr1->next;
			}
			if(ptr2->data==value)
			{
				ptr1->next=ptr2->next;
				return true;
			}
			else
				return false;

		}

	}
	void Reverse()
	{
		Node<T>*str=head;
		if(str==NULL||str->next==NULL)
			return;
		else
		{
			int temp=1;
			Node<T>*str2=head;
			while(str2->next!=NULL)
			{
				str2=str2->next;
				temp++;
			}

			T swap;
			while(str!=str2&&str2->next!=str)
			{
				swap=str->data;
				str->data=str2->data;
				str2->data=swap;

				temp--;
				str2=head;
				str=str->next;
				for(int i=1;i<temp;i++)
					str2=str2->next;
			}


		}
	}
	void Print()
	{
		Node<T>* temp=head;
		while(temp!=NULL)
		{
			cout<<temp->data<<' ';
			temp=temp->next;
		}

	}
	~LList()
	{
	}
};