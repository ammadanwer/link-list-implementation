template <typename T>
class Node
{
	
private:
	T data;
	Node<T>* next;		//points to the next node
	Node<T>* below;		//points to the node below
	
public:

	Node()					//default constructor
	{
		next=NULL;
		below=NULL;
	}
	Node(T temp)			//parameter constructor
	{
		data = temp;
		next = NULL;
		below = NULL;
	}
	
	template <typename T> friend class LList;
	template <typename T> friend class matrix;
	~Node()
	{
	}
};
